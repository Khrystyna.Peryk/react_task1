import React, { useState } from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import CreateCourse from '../CreateCourse/CreateCourse';
import Button from '../../common/Button/Button';

import {
	mockedCoursesList,
	buttonText,
	mockedAuthorsList,
} from '../../constants';

import { authorById } from '../../helpers/authorById';
import { dateGenerator } from '../../helpers/dateGenerator';
import { pipeDuration } from '../../helpers/pipeDuration';

const Courses = () => {
	const [courses, setCourses] = useState(mockedCoursesList);
	const [query, setQuery] = useState('');
	const [showCreateCourse, setShowCreateCourse] = useState(false);
	const [authorsList, setAuthorsList] = useState(mockedAuthorsList);
	const [filteredCourses, setFilteredCourses] = useState([]);

	const transferNewCourse = (courseData) => {
		setCourses((courses) => [...courses, courseData]);
	};

	const transferAuthors = (newAuthorsList) => {
		setAuthorsList(newAuthorsList);
	};

	const showCourses = () => {
		setShowCreateCourse(false);
	};

	const filterBy = () => {
		const newCourses = courses.filter((course) => {
			if (
				course.title.toLocaleLowerCase().includes(query.toLocaleLowerCase())
			) {
				return course;
			} else if (
				course.id.toLocaleLowerCase().includes(query.toLocaleLowerCase())
			) {
				return course;
			} else {
				return null;
			}
		});
		setFilteredCourses(newCourses);
	};

	const displayCreateCourse = () => setShowCreateCourse(!showCreateCourse);

	if (!showCreateCourse) {
		return (
			<>
				<div className='row'>
					<div className='col'>
						<SearchBar
							onClick={() => filterBy()}
							onChange={(event) => setQuery(event.target.value)}
							value={query}
						/>
					</div>
					<div className='col d-flex flex-row-reverse align-items-center'>
						<Button
							buttonText={buttonText.courses}
							onClick={displayCreateCourse}
							type='button'
						/>
					</div>
				</div>
				<div>
					{filteredCourses.length === 0
						? courses.map((course) => {
								const {
									id,
									title,
									description,
									creationDate,
									duration,
									authors,
								} = course;
								return (
									<div key={id}>
										<CourseCard
											title={title}
											description={description}
											creationDate={dateGenerator(creationDate)}
											duration={pipeDuration(duration)}
											authors={authorById(authors, authorsList)}
										/>
									</div>
								);
						  })
						: filteredCourses.map((course) => {
								const {
									id,
									title,
									description,
									creationDate,
									duration,
									authors,
								} = course;
								return (
									<div key={id}>
										<CourseCard
											title={title}
											description={description}
											creationDate={dateGenerator(creationDate)}
											duration={pipeDuration(duration)}
											authors={authorById(authors, authorsList)}
										/>
									</div>
								);
						  })}
				</div>
			</>
		);
	} else {
		return (
			<CreateCourse
				transferNewCourse={transferNewCourse}
				transferAuthors={transferAuthors}
				showCourses={showCourses}
			/>
		);
	}
};

export default Courses;
