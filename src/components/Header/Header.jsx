import React from 'react';
import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { myName, buttonText } from '../../constants';

const Header = () => {
	return (
		<div className='d-flex justify-content-between align-items-center'>
			<Logo />
			<div className='container text-end'>
				<b>{myName}</b>
			</div>
			<Button buttonText={buttonText.header} />
		</div>
	);
};

export default Header;
